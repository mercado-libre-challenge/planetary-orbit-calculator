# planetary-orbit-calculator

Job desarrollado en Node.JS para el ml-challenge.

### About

- Job escrito en TypeScript. Diseñada para trabajar con MongoDB. Base de datos alojada en [atlas](https://www.mongodb.com/cloud/atlas).
- Conección con la base de datos [Mongoose ODM](https://mongoosejs.com/)
- Test realizados con [mochajs](https://mochajs.org/) y [chaijs](https://www.chaijs.com/)

### Supuestos

- Se realizaron test con covertura total pero "superficial"; es decir, no se terminaron de probar todas las posibilidades de si para X no puede pasar Y, etc.
- Los test se realizan sobre una base de datos igual pero no la misma que la "productiva". Productiva: planetary_system, Test: planetary_system_test.
- No usar la base de tests como persistencia ya que se borran todos los datos en cada run de test.

### Planetary System

##### Planets

- El planeta Ferengi se desplaza con una velocidad angular de 1 grados/día en sentido
horario. Su distancia con respecto al sol es de 500Km.
- El planeta Betasoide se desplaza con una velocidad angular de 3 grados/día en sentido
horario. Su distancia con respecto al sol es de 2000Km.
- El planeta Vulcano se desplaza con una velocidad angular de 5 grados/día en sentido
anti­horario, su distancia con respecto al sol es de 1000Km.

##### Weather

- Dirt (1): Cuando los tres planetas están alineados entre sí y a su vez alineados con respecto al sol, el sistema solar experimenta un período de sequía.
- Rain (2), Max Rain (3): Cuando los tres planetas no están alineados, forman entre sí un triángulo. Es sabido que en el momento en el que el sol se encuentra dentro del triángulo, el sistema solar experimenta un  período de lluvia, teniendo éste, un pico de intensidad cuando el perímetro del triángulo está en su máximo. 
- Fine (4): Las condiciones óptimas de presión y temperatura se dan cuando los tres planetas están alineados entre sí pero no están alineados con el sol.


### Features

- Se crean los planetas del sistema planetario.
- Se crea el sitema planetario calculando las posiciones y condiciones del día 0.
- Se calcula el sistema para los días indicados. Se obtiene un registro por cada día del sistema donde se calculan las órbitas y el estado del clima del sistema.
- Una vez el sistema está calculado se obtiene toda la información y se generan las estádisticas que nos permiten saber: Cuantos periódos de cada clima habrá y qué días serán los de máxima lluvia.


### Install

```
git clone https://gitlab.com/mercado-libre-challenge/planetary-orbit-calculator.git
npm install
```

### Run

```
npm run-script build

npm start
```

Checkear las variables de entorno



### Testing

To run the tests:

```
npm test
```

### Author

- [Rodrigo Calvo](https://gitlab.com/recalvo)
- [Consultas](mailto:rodrigocalvo95@gmail.com)

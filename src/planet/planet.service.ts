/**
 * Data Model Interfaces
 */

import { Planet, PlanetModel } from "./planet.class";
import { Cache } from "./../common/cache.service";

const cache: Cache<Planet> = new Cache<Planet>();

/**
 * Service Methods
 */

export default class PlanetClass {
  public findAll = async (): Promise<Array<Planet>> => {
    return await PlanetModel.find();
  }

  public find = async (id: string): Promise<Planet> => {
    let record: Planet = cache.get(id);

    if (!record) {
      record = await PlanetModel.findById(id);

      if (record) {
        cache.put(id, record);
      }
    }

    if (record) {
      return record;
    }

    throw new Error("No record found");
  }

  public findFew = async (ids: Array<string>): Promise<Array<Planet>> => {
    const records: Array<Planet> = await PlanetModel.find({
      "_id": { $in: ids }
    });

    if (records) {
      return records;
    }

    throw new Error("No records found");
  }

  public create = async (newRecord: Planet): Promise<Planet> => {
    return await PlanetModel.create(newRecord);
  }

  public update = async (
    id: string,
    updatedRecord: Planet
  ): Promise<Planet> => {
    const record: Planet | null = await PlanetModel.findByIdAndUpdate(
      { _id: id },
      updatedRecord,
      { new: true }
    );

    if (record) {
      cache.put(id, record);
      return record;
    }

    throw new Error("No record found to update");
  }

  public remove = async (id: string): Promise<void> => {
    const del: any = await PlanetModel.deleteOne({ _id: id });

    if (del && del.deletedCount) {
      cache.delete(id);
      return;
    }

    throw new Error("No record found to delete");
  }
}
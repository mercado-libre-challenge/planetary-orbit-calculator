/**
 * Data Model Interfaces
 */

import { System, SystemModel } from "./system.class";
import { Orbit } from "./orbit.class";
import { Weather } from "./weather.class";
import { Planet } from "./../planet/planet.class";
import { Cache } from "./../common/cache.service";

const cache: Cache<System> = new Cache<System>();

/**
 * Service Methods
 */

export default class SystemClass {
  public findAll = async (): Promise<Array<System>> => {
    return await SystemModel.find();
  }

  public find = async (id: string): Promise<System> => {
    let record: System = cache.get(id);

    if (!record) {
      record = await SystemModel.findById(id);

      if (record) {
        cache.put(id, record);
      }
    }

    if (record) {
      return record;
    }

    throw new Error("No record found");
  }

  public create = async (planets: Array<Planet>): Promise<System> => {
    const system: System = new System();
    system.day = 0;

    system.orbits = new Array<Orbit>();
    planets.forEach(planet => {
      system.orbits!.push(new Orbit({
        planet: planet,
        position: 0
      }));
    });

    system.weather = await system.calculateWeather(system.orbits);

    return await SystemModel.create(system);
  }

  public update = async (
    id: string,
    updatedRecord: System
  ): Promise<System> => {
    const record: System | null = await SystemModel.findByIdAndUpdate(
      { _id: id },
      updatedRecord,
      { new: true }
    );

    if (record) {
      cache.put(id, record);
      return record;
    }

    throw new Error("No record found to update");
  }

  public remove = async (id: string): Promise<void> => {
    const del: any = await SystemModel.deleteOne({ _id: id });

    if (del && del.deletedCount) {
      cache.delete(id);
      return;
    }

    throw new Error("No record found to delete");
  }

  public findByDay = async (day: number): Promise<System> => {
    let record: System = cache.get(day.toString());

    if (!record) {
      record = await SystemModel.findOne({ day: day });

      if (record) {
        cache.put(day.toString(), record);
      }
    }

    if (record) {
      return record;
    }

    throw new Error("No record found");
  }

  public getCountByWeather = async (weather: Weather): Promise<number> => {
    return await SystemModel.countDocuments({ weather: weather });
  }

  public getByWeather = async (weather: Weather): Promise<Array<System>> => {
    return await SystemModel.find({ weather: weather });
  }

  public calculateSystem = async (days: number): Promise<void> => {
    /*
    El sistema se va calculando día a día sobre si mismo. Se toma el día 1 y se calcula el día 2. Se toma el día 2 y se calcula el 3 y así.
    */
    const base_system: System = await this.findByDay(0);

    if (!base_system) {
      throw new Error("System not created");
    }

    let current_day: System = base_system;
    for (let i: number = 1; i <= days; i++) {
      const next_day_system: System = await current_day.calculateNextDay();

      await SystemModel.create(next_day_system);

      current_day = next_day_system;
    }
  }
}
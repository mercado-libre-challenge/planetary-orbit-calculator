import { index, prop, getModelForClass, Ref } from "@typegoose/typegoose";
import { Orbit } from "./orbit.class";
import { Weather } from "./weather.class";
import { Planet } from "./../planet/planet.class";
import PlanetService from "./../planet/planet.service";
import { sin, are_the_points_in_the_same_line, is_zero_in_triangle, get_perimeter } from "../common/trigonometry.service";

const planetService: PlanetService = new PlanetService();

@index({ day: 1 }, { unique: true })
export class System {
  @prop({ required: true, default: 0 })
  public day?: number;

  @prop()
  public orbits?: Array<Orbit>;

  @prop({ required: true })
  public weather?: Weather;

  // calculamos el próximo día a partir del actual.
  async calculateNextDay(): Promise<System> {
    const system: System = new System();
    system.day = this.day! + 1;

    system.orbits = new Array<Orbit>();
    for (const _orbit of this.orbits!) {
      system.orbits!.push(await new Orbit(_orbit).calculateNextDay());
    }

    system.weather = await system.calculateWeather(system.orbits);

    return system;
  }

  public async getPerimeter(): Promise<number> {
    // calculamos el perímetro para saber el día max de lluvia
    const planets_position: Array<any> = [];

    for (const _orbit of this.orbits!) {
      const planet: Planet = await planetService.find((_orbit.planet as any).toString());
      planets_position.push({
        star_distance: planet.star_distance,
        position: _orbit.position
      });
    }

    return get_perimeter(planets_position);
  }

  public async calculateWeather(orbits: Orbit[]): Promise<Weather> {
    const planets_position: Array<any> = [];

    for (const _orbit of orbits) {
      const planet: Planet = await planetService.find((_orbit.planet as any).toString());
      planets_position.push({
        star_distance: planet.star_distance,
        position: _orbit.position
      });
    }

    return this.calculate(planets_position);
  }

  private calculate(planets_positions: Array<any>): Weather {
    let weather: Weather = Weather.max_rain;

    // weather.dirt
    // si el valor absoluto de los senos de todos los angulos es el mismo quiere decir que estan en una linea recta con respecto
    // de la estrella.
    if (planets_positions.every((val, i, arr) => sin(val.position) === sin(arr[0].position))) {
      weather = Weather.dirt;
    } else if (are_the_points_in_the_same_line(planets_positions)) {
      weather = Weather.fine;
    } else if (is_zero_in_triangle(planets_positions)) {
      weather = Weather.rain;
    }

    return weather;
  }
}

export const SystemModel: any = getModelForClass(System);

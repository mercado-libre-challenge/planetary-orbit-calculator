import { prop, Ref } from "@typegoose/typegoose";
import { Planet } from "./../planet/planet.class";
import PlanetService from "./../planet/planet.service";
import mongoose from "mongoose";

export class Orbit {
    @prop({ ref: "Planet", refType: mongoose.Schema.Types.ObjectId, required: true })
    public planet?: Ref<Planet>;

    @prop({ required: true, default: 0 })
    public position?: number;

    // calculamos el próximo día a partir del actual.
    async calculateNextDay(): Promise<Orbit> {
        const planet: Planet = await new PlanetService().find(this.planet as string);

        let velocity: number = planet.angular_velocity! > 0 ? planet.angular_velocity! : 360 + planet.angular_velocity!;

        let position: number = this.position! + velocity;

        if (position >= 360) {
            position -= 360;
        }

        return new Orbit({
            planet: this.planet,
            position: position
        });
    }

    constructor(data?: any) {
        if (data) {
            this.planet = data!.planet._id || data!.planet || null;
            this.position = data!.position || 0;
        }
    }
}
import MongoDB from "./common/dbconnector";
import * as dotenv from "dotenv";
import PlanetService from "./planet/planet.service";
import SystemService from "./system/system.service";
import StatisticClass from "./statistic/statistic.service";
import { Planet } from "./planet/planet.class";
import { mongoose } from "@typegoose/typegoose";

dotenv.config();

const mongoDB: MongoDB = new MongoDB();
mongoDB.connect();

/*
Funcionamiento del script:
- Ejecutamos create_system() y allí se borrará el posible sistema actual, se crearan los planetas
y se creará el sistema actual en su dia inicial
- Luego se calculará el sistema para los días indicados en el archivo .env
- El sistema se va calculando día a día sobre si mismo. Se toma el día 1 y se calcula el día 2. Se toma el día 2 y se calcula el 3 y así.
- Al final se calculan las estadísticas en base al sistema calculado.
*/


const create_planets = async (): Promise<void> => {
    const planetService: PlanetService = new PlanetService();

    const planet_1: Planet = new Planet({
        name: "Ferengi",
        angular_velocity: 1,
        star_distance: 500
    });

    const planet_2: Planet = new Planet({
        name: "Betasoide",
        angular_velocity: 3,
        star_distance: 2000
    });

    const planet_3: Planet = new Planet({
        name: "Vulcano",
        angular_velocity: -5,
        star_distance: 1000
    });

    await planetService.create(planet_1);
    await planetService.create(planet_2);
    await planetService.create(planet_3);

    return;
};

const delete_actual_system = async (): Promise<void> => {
    try {
        await mongoose.connection.collections.planets.drop();
    } catch { }
    try {
        await mongoose.connection.collections.systems.drop();
    } catch { }
    try {
        await mongoose.connection.collections.statistics.drop();
    } catch { }
    return;
};

const create_system = async (): Promise<void> => {
    await delete_actual_system();

    const planetService: PlanetService = new PlanetService();
    const systemService: SystemService = new SystemService();
    const statisticClass: StatisticClass = new StatisticClass();

    await create_planets();

    const planets: Array<any> = await planetService.findAll();

    await systemService.create(planets.map(p => p._id));

    // await systemService.calculateSystem(3650);
    await systemService.calculateSystem(parseInt(process.env.DAYS_TO_CALCULATE as string));

    await statisticClass.calculate();

    // terminamos el proceso.
    process.exit(0);
};

create_system();

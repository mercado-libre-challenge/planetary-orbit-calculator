/**
 * Data Model Interfaces
 */

import { System, SystemModel } from "./../system/system.class";
import { Statistic, StatisticModel } from "./statistic.class";
import SystemClass from "../system/system.service";
import { Weather } from "../system/weather.class";

/**
 * Service Methods
 */

export default class StatisticClass {

    public get = async (): Promise<Statistic> => {
        return await StatisticModel.findOne();
    }

    public calculate = async (): Promise<void> => {
        // buscamos la cantidad de días por clima y luego calculamos los días de máxima lluvia.
        if (await this.get()) {
            return;
        }

        const systemClass: SystemClass = new SystemClass();

        const dirty_periods_count: number = await systemClass.getCountByWeather(Weather.dirt);
        const fine_periods_count: number = await systemClass.getCountByWeather(Weather.fine);
        const rain_periods_count: number = await systemClass.getCountByWeather(Weather.rain);
        const max_rain_periods_count: number = await systemClass.getCountByWeather(Weather.max_rain);

        let days_max_rain: Array<number> = [];
        const max_rain_situations: Array<System> = await systemClass.getByWeather(Weather.max_rain);

        if (max_rain_situations && max_rain_situations.length) {
            let max_perimeter: number = 0;
            for (let index: number = 0; index < max_rain_situations.length; index++) {
                const system: System = max_rain_situations[index];

                let this_system_perimeter: number = await system.getPerimeter();

                if (this_system_perimeter > max_perimeter) {
                    max_perimeter = this_system_perimeter;
                }
            }

            for (let index: number = 0; index < max_rain_situations.length; index++) {
                const system: System = max_rain_situations[index];

                let this_system_perimeter: number = await system.getPerimeter();

                if (this_system_perimeter === max_perimeter) {
                    const day: number = system.day || 0;
                    if (day > 0) {
                        days_max_rain.push(day);
                    }
                }
            }
        }

        await StatisticModel.create(new Statistic({
            dirty_periods_count: dirty_periods_count,
            fine_periods_count: fine_periods_count,
            rain_periods_count: rain_periods_count,
            max_rain_periods_count: max_rain_periods_count,
            days_max_rain: days_max_rain,
        }));

        return;
    }
}
// de angulo decimal a radianes
function toRadians(angle: number): number {
    return angle * (Math.PI / 180);
}

// lado para calcular perímetro o para calcular area
function get_side(dot_1: any, dot_2: any): number {
    const side: number = parseFloat(
        Math.sqrt(
            Math.pow(dot_2.x - dot_1.x, 2) + Math.pow(dot_2.y - dot_1.y, 2)
        ).toFixed(6)
    );

    return side;
}

function sign(p1: any, p2: any, p3: any): number {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

// identifico si un punto está dentro de un triángulo
function is_point_in_triangle(pt: any, v1: any, v2: any, v3: any): boolean {
    let d1: number = sign(pt, v1, v2);
    let d2: number = sign(pt, v2, v3);
    let d3: number = sign(pt, v3, v1);

    let has_neg: boolean = d1 < 0 || d2 < 0 || d3 < 0;
    let has_pos: boolean = d1 > 0 || d2 > 0 || d3 > 0;

    return !(has_neg && has_pos);
}

// A partir de el ángulo y de la distancia al centro calculo el punto x y
function get_dot(angle: number, h: number): any {
    let result: any = {};

    if (angle < 90) {
        result.x = cos(angle) * h;
        result.y = sin(angle) * h * -1;
    } else if (angle === 90) {
        result.x = 0;
        result.y = h * -1;
    } else if (angle < 180) {
        angle = angle - 90;

        result.x = sin(angle) * h * -1;
        result.y = cos(angle) * h * -1;
    } else if (angle === 180) {
        result.x = h * -1;
        result.y = 0;
    } else if (angle < 270) {
        angle = angle - 180;

        result.x = cos(angle) * h * -1;
        result.y = sin(angle) * h;
    } else if (angle === 270) {
        result.x = 0;
        result.y = h;
    } else {
        angle = angle - 270;

        result.x = sin(angle) * h;
        result.y = cos(angle) * h;
    }

    result.x = parseFloat(result.x.toFixed(6));
    result.y = parseFloat(result.y.toFixed(6));

    return result;
}

// seno de un angulo
export function sin(angle: number): number {
    return parseFloat(Math.abs(Math.sin(toRadians(angle))).toFixed(6));
}

// coseno de un angulo
export function cos(angle: number): number {
    return parseFloat(Math.abs(Math.cos(toRadians(angle))).toFixed(6));
}

// calculo si los 3 puntos o planetas están en una linea recta o de la misma pendiente
export function are_the_points_in_the_same_line(data: Array<any>): boolean {
    // doc: https://www.algebra.com/algebra/homework/Length-and-distance/Length-and-distance.faq.question.530663.html
    if (data.length < 3) {
        return true;
    }

    let dots: Array<any> = [];

    data.forEach((e) => {
        dots.push(get_dot(e.position, e.star_distance));
    });

    // we are going to see the slope of the first two dots.
    let dot_1: any = dots[0];
    let dot_2: any = dots[1];

	/*
    Recall the three points are: (-3,4) (3,2) (6,1).
    This time let's use (3,2) (6,1) to define the line and (-3,4) to find 'b'.
    .
    m = (2-1)/(3-6) = 1/-3 = -1/3.
    */

    let m: number = parseFloat(((dot_1.y - dot_2.y) / (dot_1.x - dot_2.x)).toFixed(4));

    // now if only 1 of the remains points dont fit they are not in the same line.
    for (let index: number = 2; index < dots.length; index++) {
        const dot_index: any = dots[index];

        let aux_m: number = parseFloat(((dot_1.y - dot_index.y) / (dot_1.x - dot_index.x)).toFixed(4));

        if (aux_m !== m) {
            return false;
        }
    }

    return true;
}

// identificamos si el sol está dentro de los planetas
export function is_zero_in_triangle(data: Array<any>): boolean {
    if (data.length !== 3) {
        return false;
    }

    const zero: any = { x: 0, y: 0 };

    const dot_1: any = get_dot(
        data[0].position,
        data[0].star_distance
    );
    const dot_2: any = get_dot(
        data[1].position,
        data[1].star_distance
    );
    const dot_3: any = get_dot(
        data[2].position,
        data[2].star_distance
    );

    return is_point_in_triangle(zero, dot_1, dot_2, dot_3);
}

// calculamos perimetro
export function get_perimeter(data: Array<any>): number {
    const dot_1: any = get_dot(data[0].position, data[0].star_distance);
    const dot_2: any = get_dot(data[1].position, data[1].star_distance);
    const dot_3: any = get_dot(data[2].position, data[2].star_distance);

    const perimeter: number =
        get_side(dot_1, dot_2) + get_side(dot_1, dot_3) + get_side(dot_2, dot_3);

    return perimeter;
}
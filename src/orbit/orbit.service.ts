/**
 * Data Model Interfaces


import { Orbit, OrbitModel } from "./orbit.class";

/**
 * Service Methods


export default class OrbitClass {
  public findAll = async (): Promise<Array<Orbit>> => {
    return await OrbitModel.find();
  };

  public find = async (id: string): Promise<Orbit> => {
    const record: Orbit | null = await OrbitModel.findById(id);

    if (record) {
      return record;
    }

    throw new Error("No record found");
  };

  public create = async (newRecord: Orbit): Promise<Orbit> => {
    return await OrbitModel.create(newRecord);
  };

  public update = async (
    id: string,
    updatedRecord: Orbit
  ): Promise<Orbit> => {
    const record: Orbit | null = await OrbitModel.findByIdAndUpdate(
      { _id: id },
      updatedRecord,
      { new: true }
    );

    if (record) {
      return record;
    }

    throw new Error("No record found to update");
  };

  public remove = async (id: string): Promise<void> => {
    const del: any = await OrbitModel.deleteOne({ _id: id });

    if (del && del.deletedCount) {
      return;
    }

    throw new Error("No record found to delete");
  };
}
*/
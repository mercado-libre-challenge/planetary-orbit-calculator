import { expect } from "chai";
import PlanetService from "./../src/planet/planet.service";
import { Planet } from "./../src/planet/planet.class";

const planetService: PlanetService = new PlanetService();

let working_planet_id: string = "";

describe("PlanetService", () => {
    before((done) => {
        require("./test_helper");

        const planet: Planet = new Planet({
            name: "La tierra media",
            angular_velocity: 10,
            star_distance: 999
        });

        planetService.create(planet).then((u: any) => {
            working_planet_id = u._id;
            done();
        });
    });
    describe("add", () => {
        it("should work", (done) => {
            const planet: Planet = new Planet({
                name: "Marte",
                angular_velocity: 1,
                star_distance: 2
            });

            planetService.create(planet).then((u: any) => {
                expect(u).to.have.property("_id");
                done();
            });
        });
        it("should not work - repeat name", (done) => {
            const planet: Planet = new Planet({
                name: "Jupiter",
                angular_velocity: 5,
                star_distance: 150
            });
            planetService.create(planet).then((u: any) => {
                expect(u).to.have.property("_id");

                planetService.create(planet).then((_u: any) => {
                    expect(_u).to.not.have.property("_id");
                    done();
                }).catch((err: any) => {
                    expect(err).to.not.equals(undefined);
                    done();
                });
            });
        });
    });
    it("findAll", (done) => {
        planetService.findAll().then((planets: Array<Planet>) => {
            expect(planets).to.be.a("Array");
            done();
        });
    });
    it("find", (done) => {
        planetService.find(working_planet_id).then((planet: Planet) => {
            expect(planet.name).to.equal("La tierra media");
            expect(planet.angular_velocity).to.equal(10);
            done();
        });
    });
    it("findFew", (done) => {
        planetService.findFew([working_planet_id]).then((planets: Array<Planet>) => {
            expect(planets).to.be.a("Array");
            done();
        });
    });
    it("update", (done) => {
        planetService.find(working_planet_id).then((planet: Planet) => {
            expect(planet.name).to.equal("La tierra media");

            planet.name = "Urano";
            planetService.update(working_planet_id, planet).then((_planet: Planet) => {
                expect(_planet.name).to.equal("Urano");

                _planet.name = "La tierra media";
                planetService.update(working_planet_id, _planet).then((__planet: Planet) => {
                    expect(__planet.name).to.equal("La tierra media");
                    done();
                });
            });
        });
    });
    it("remove", (done) => {
        planetService.remove(working_planet_id).then((_u: any) => {
            expect(_u).to.be.a("undefined");
            done();
        });
    });
});
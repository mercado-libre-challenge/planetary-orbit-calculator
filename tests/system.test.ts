import { expect } from "chai";
import PlanetService from "./../src/planet/planet.service";
import { Planet } from "./../src/planet/planet.class";
import SystemService from "./../src/system/system.service";
import StatisticService from "./../src/statistic/statistic.service";
import { System } from "./../src/system/system.class";
import { Weather } from "../src/system/weather.class";
import { Statistic } from "../src/statistic/statistic.class";

const planetService: PlanetService = new PlanetService();
const systemService: SystemService = new SystemService();
const statisticService: StatisticService = new StatisticService();

let working_planet_id_1: string = "";
let working_planet_id_2: string = "";
let working_planet_id_3: string = "";
let working_system_id: string = "";

describe("SystemService", () => {
    before((done) => {
        require("./test_helper");

        const planet_1: Planet = new Planet({
            name: "Planeta 1",
            angular_velocity: 10,
            star_distance: 500
        });

        const planet_2: Planet = new Planet({
            name: "Planeta 2",
            angular_velocity: 20,
            star_distance: 750
        });

        const planet_3: Planet = new Planet({
            name: "Planeta 3",
            angular_velocity: 30,
            star_distance: 1000
        });

        planetService.create(planet_1).then((u: any) => {
            working_planet_id_1 = u._id;
            planetService.create(planet_2).then((u: any) => {
                working_planet_id_2 = u._id;
                planetService.create(planet_3).then((u: any) => {
                    working_planet_id_3 = u._id;
                    done();
                });
            });
        });
    });
    it("create", (done) => {
        planetService.findFew([working_planet_id_1, working_planet_id_2, working_planet_id_3]).then((planets: Array<Planet>) => {
            expect(planets).to.be.a("Array");
            systemService.create(planets).then((system: any) => {
                expect(system).to.have.property("_id");
                working_system_id = system._id;
                done();
            });
        });
    });
    it("findAll", (done) => {
        systemService.findAll().then((systems: Array<System>) => {
            expect(systems).to.be.a("Array");
            done();
        });
    });
    it("find", (done) => {
        systemService.find(working_system_id).then((system: System) => {
            expect(system.day).to.equal(0);
            done();
        });
    });
    it("findByDay", (done) => {
        systemService.findByDay(0).then((system: System) => {
            expect(system.day).to.equal(0);
            done();
        });
    });
    it("update", (done) => {
        systemService.find(working_system_id).then((system: System) => {
            expect(system.day).to.equal(0);

            system.day = 1;
            systemService.update(working_system_id, system).then((_system: System) => {
                expect(_system.day).to.equal(1);

                _system.day = 0;
                systemService.update(working_system_id, _system).then((__system: System) => {
                    expect(__system.day).to.equal(0);
                    done();
                });
            });
        });
    });
    it("calculateSystem", (done) => {
        systemService.calculateSystem(10).then((_u: void) => {
            expect(_u).to.be.a("undefined");

            systemService.findByDay(10).then((system: System) => {
                expect(system.day).to.equal(10);
                expect(system.weather).to.equal(Weather.rain);

                statisticService.calculate().then((s: void) => {
                    statisticService.get().then((statistic: Statistic) => {
                        expect(statistic.max_rain_periods_count).to.equal(8);
                        expect(statistic.days_max_rain![0] || 0).to.equal(8);
                        done();
                    });
                });
            });
        });
    });
    it("remove", (done) => {
        systemService.remove(working_system_id).then((_u: any) => {
            expect(_u).to.be.a("undefined");
            done();
        });
    });
});
function are_the_point_in_the_same_line(data) {
	//Doc: https://www.algebra.com/algebra/homework/Length-and-distance/Length-and-distance.faq.question.530663.html
	if (data.length < 3) {
		return true;
	}

	/*
	let dots = [];

	data.forEach((e) => {
		dots.push(get_dot(e.position, e.star_distance));
	});
	*/
	let dots = [
		{x: -3, y: 4},
		{x: 3, y: 2},
		{x: 6, y: 1}
	];

	console.log(dots);

	//We are going to see the slope of the first two dots.
	let dot_1 = dots[0];
	let dot_2 = dots[1];

	/*
    Recall the three points are: (-3,4) (3,2) (6,1).
    This time let's use (3,2) (6,1) to define the line and (-3,4) to find 'b'.
    .
    m = (2-1)/(3-6) = 1/-3 = -1/3.
    */

	let m = (dot_1.y - dot_2.y) / (dot_1.x - dot_2.x);

	console.log("slope", m);

	//Now if only 1 of the remains points dont fit they are not in the same line.
	for (let index = 2; index < dots.length; index++) {
		const dot_index = dots[index];

		let aux_m = (dot_1.y - dot_index.y) / (dot_1.x - dot_index.x);

		if (aux_m !== m) {
			return false;
		}
	}

	return true;
}

function get_dot(angle, h) {
	console.log("angle", angle, "h", h);

	let result = {};
	if (angle < 90) {
		result.x = cos(angle) * h;
		result.y = sin(angle) * h * -1;
	} else if (angle === 90) {
		result.x = 0;
		result.y = h * -1;
	} else if (angle < 180) {
		angle = angle - 90;

		result.x = sin(angle) * h * -1;
		result.y = cos(angle) * h * -1;
	} else if (angle === 180) {
		result.x = h * -1;
		result.y = 0;
	} else if (angle < 270) {
		angle = angle - 180;

		result.x = cos(angle) * h * -1;
		result.y = sin(angle) * h;
	} else if (angle === 270) {
		result.x = 0;
		result.y = h;
	} else {
		angle = angle - 270;

		result.x = sin(angle) * h;
		result.y = cos(angle) * h;
	}

	return result;
}

function toRadians(angle) {
	return angle * (Math.PI / 180);
}
function sin(angle) {
	return parseFloat(Math.abs(Math.sin(toRadians(angle))).toFixed(6));
}
function cos(angle) {
	return parseFloat(Math.abs(Math.cos(toRadians(angle))).toFixed(6));
}

const planets_positions = [];

planets_positions.push({
	star_distance: 20,
	position: 0
});
planets_positions.push({
	star_distance: 20,
	position: 0
});
planets_positions.push({
	star_distance: 20,
	position: 0
});

/*
console.log(
	sin(planets_positions[0].position),
	cos(planets_positions[0].position)
);
console.log(
	sin(planets_positions[1].position),
	cos(planets_positions[1].position)
);
console.log(
	sin(planets_positions[2].position),
	cos(planets_positions[2].position)
);
*/

if (are_the_point_in_the_same_line(planets_positions)) {
	console.log("are in the same");
}

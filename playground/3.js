function sign(p1, p2, p3) {
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

function is_point_in_triangle(pt, v1, v2, v3) {
	let d1, d2, d3;
	let has_neg, has_pos;

	d1 = sign(pt, v1, v2);
	d2 = sign(pt, v2, v3);
	d3 = sign(pt, v3, v1);

	has_neg = d1 < 0 || d2 < 0 || d3 < 0;
	has_pos = d1 > 0 || d2 > 0 || d3 > 0;

	return !(has_neg && has_pos);
}
function is_zero_in_triangle(planets_positions) {
	let zero = {x: 0, y: 0};
	let dot_1 = get_dot(
		planets_positions[0].position,
		planets_positions[0].star_distance
	);
	let dot_2 = get_dot(
		planets_positions[1].position,
		planets_positions[1].star_distance
	);
	let dot_3 = get_dot(
		planets_positions[2].position,
		planets_positions[2].star_distance
	);

	return is_point_in_triangle(zero, dot_1, dot_2, dot_3);
}

function get_dot(angle, h) {
	console.log("angle", angle, "h", h);

	let result = {};
	if (angle < 90) {
		result.x = cos(angle) * h;
		result.y = sin(angle) * h * -1;
	} else if (angle === 90) {
		result.x = 0;
		result.y = h * -1;
	} else if (angle < 180) {
		angle = angle - 90;

		result.x = sin(angle) * h * -1;
		result.y = cos(angle) * h * -1;
	} else if (angle === 180) {
		result.x = h * -1;
		result.y = 0;
	} else if (angle < 270) {
		angle = angle - 180;

		result.x = cos(angle) * h * -1;
		result.y = sin(angle) * h;
	} else if (angle === 270) {
		result.x = 0;
		result.y = h;
	} else {
		angle = angle - 270;

		result.x = sin(angle) * h;
		result.y = cos(angle) * h;
	}

	return result;
}
function toRadians(angle) {
	return angle * (Math.PI / 180);
}
function sin(angle) {
	return parseFloat(Math.abs(Math.sin(toRadians(angle))).toFixed(6));
}
function cos(angle) {
	return parseFloat(Math.abs(Math.cos(toRadians(angle))).toFixed(6));
}

const planets_positions = [];

planets_positions.push({
	star_distance: 20,
	position: 110
});
planets_positions.push({
	star_distance: 20,
	position: 220
});
planets_positions.push({
	star_distance: 20,
	position: 30
});

if (is_zero_in_triangle(planets_positions)) {
	console.log("zero is in the middle");
}

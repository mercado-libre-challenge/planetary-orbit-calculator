function toRadians(angle) {
	return angle * (Math.PI / 180);
}
function sin(angle) {
	return parseFloat(Math.abs(Math.sin(toRadians(angle))).toFixed(6));
}

const planets_positions = [];

planets_positions.push({
	star_distance: 20,
	position: 0
});
planets_positions.push({
	star_distance: 20,
	position: 0
});
planets_positions.push({
	star_distance: 20,
	position: 0
});

console.log(sin(planets_positions[0].position));
console.log(sin(planets_positions[1].position));
console.log(sin(planets_positions[2].position));

if (
	planets_positions.every(
		(val, i, arr) => sin(val.position) === sin(arr[0].position)
	)
) {
	console.log("se");
}

function sign(p1, p2, p3) {
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}
function get_dot(angle, h) {
	console.log("angle", angle, "h", h);

	let result = {};
	if (angle < 90) {
		result.x = cos(angle) * h;
		result.y = sin(angle) * h * -1;
	} else if (angle === 90) {
		result.x = 0;
		result.y = h * -1;
	} else if (angle < 180) {
		angle = angle - 90;

		result.x = sin(angle) * h * -1;
		result.y = cos(angle) * h * -1;
	} else if (angle === 180) {
		result.x = h * -1;
		result.y = 0;
	} else if (angle < 270) {
		angle = angle - 180;

		result.x = cos(angle) * h * -1;
		result.y = sin(angle) * h;
	} else if (angle === 270) {
		result.x = 0;
		result.y = h;
	} else {
		angle = angle - 270;

		result.x = sin(angle) * h;
		result.y = cos(angle) * h;
	}

	result.x = parseFloat(result.x.toFixed(6));
	result.y = parseFloat(result.y.toFixed(6));

	return result;
}
function toRadians(angle) {
	return angle * (Math.PI / 180);
}
function sin(angle) {
	return parseFloat(Math.abs(Math.sin(toRadians(angle))).toFixed(6));
}
function cos(angle) {
	return parseFloat(Math.abs(Math.cos(toRadians(angle))).toFixed(6));
}

function get_side(dot_1, dot_2) {
	const side = parseFloat(
		Math.sqrt(
			Math.pow(dot_2.x - dot_1.x, 2) + Math.pow(dot_2.y - dot_1.y, 2)
		).toFixed(6)
	);
	console.log("side", side, "of", dot_1, dot_2);

	return side;
}

function get_perimeter(data) {
	const dot_1 = get_dot(data[0].position, data[0].star_distance);
	const dot_2 = get_dot(data[1].position, data[1].star_distance);
	const dot_3 = get_dot(data[2].position, data[2].star_distance);

	const perimeter =
		get_side(dot_1, dot_2) + get_side(dot_1, dot_3) + get_side(dot_2, dot_3);

	console.log("perimeter", perimeter);

	return perimeter;
}

const planets_positions = [];

planets_positions.push({
	star_distance: 20,
	position: 110
});
planets_positions.push({
	star_distance: 20,
	position: 220
});
planets_positions.push({
	star_distance: 20,
	position: 30
});

get_perimeter(planets_positions);
